package bit.minisys.minicc.ir;

import java.util.ArrayList;

public class IRIterateStatement {
	public ArrayList<IRVariableDeclaration> varNames = new ArrayList<>();
		
	public IRExpression cond;
	public IRExpression step;
	
	public IRScope body;
	
}
