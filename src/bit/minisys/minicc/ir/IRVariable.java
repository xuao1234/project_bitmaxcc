package bit.minisys.minicc.ir;

public class IRVariable {
	public String flag;				public String varType;
	public String varName;
	public int value;
	public int offset;
	public IRExpression initExpr;
}
