package bit.minisys.minicc.ir;

import java.util.ArrayList;

public class IRCompilationUnit {

		
	public ArrayList<IRFunction> functionNames;
	public ArrayList<IRVariableDeclaration> variableNames;
	public ArrayList<IRArrayDeclaration> arrayNames;
		
	public IRCompilationUnit(){
		functionNames = new ArrayList<IRFunction>();
	    variableNames = new ArrayList<IRVariableDeclaration>();
	    arrayNames = new ArrayList<IRArrayDeclaration>();
	}
	       
	public void addFunction(IRFunction fd){
		functionNames.add(fd);
	}
		
	public void addVariable(IRVariableDeclaration var) {
		variableNames.add(var);
	}
	
	public void addArray(IRArrayDeclaration array) {
		arrayNames.add(array);
	}
}
