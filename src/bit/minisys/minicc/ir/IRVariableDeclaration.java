package bit.minisys.minicc.ir;

public class IRVariableDeclaration {
	public String flag;				public String varType;
	public String llvmType;
	public String varName;
	public int value = 0;
	public int offset;
	public IRExpression initExpr;
}

