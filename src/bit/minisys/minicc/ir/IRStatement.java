package bit.minisys.minicc.ir;

public class IRStatement {
	public String statType;		
	public IRReturnStatement returnStat;
	public IRExpressionStatement exprStat;
	public IRSelectStatement selectStat;
	public IRIterateStatement iterateStat;
}
