package bit.minisys.minicc.ir;

public class IRExpression {
	public String exprType;				
	public IRConstval constval = new IRConstval();
	public IRVariable var = new IRVariable();
	
	public String op;
	public IRExpression opnd0 ;
	public IRExpression opnd1 ; 
	
	public IRFunctionCall call = new IRFunctionCall();
	 
	public void initBinary() {
		opnd0 = new IRExpression();
		opnd1 = new IRExpression();
	}
}
