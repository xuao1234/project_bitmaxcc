package bit.minisys.minicc.ir;

import java.util.ArrayList;

public class IRSelectStatement {
	
		
	public IRExpression cond;
	public boolean hasFalse ;
	public IRScope trueScope ;
	public IRScope falseScope ;
	
}
