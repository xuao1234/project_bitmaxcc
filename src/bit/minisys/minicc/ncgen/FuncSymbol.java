package bit.minisys.minicc.ncgen;

import java.util.ArrayList;

public class FuncSymbol extends Symbol{
	public String retType;
	public int parCount;
	public ArrayList<String> parTypes;
	public ArrayList<Integer> parOffsets;
	
	public FuncSymbol(){
		this.parTypes = new ArrayList<>();
	}
}
