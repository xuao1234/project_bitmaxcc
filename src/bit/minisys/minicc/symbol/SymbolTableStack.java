package bit.minisys.minicc.symbol;

import java.util.ArrayList;

import bit.minisys.minicc.symbol.FunctionSymbol;
import bit.minisys.minicc.symbol.SymbolTable;

public class SymbolTableStack {
    private ArrayList<SymbolTable> stack;
    private ArrayList<String> frameName;

    public SymbolTableStack() {
        stack = new ArrayList<>();
        frameName = new ArrayList<>();
    }

    public void appendSymbolTable(String frameName) {
        this.stack.add(new SymbolTable());
        this.frameName.add(frameName);
    }

    public SymbolTable pop() {
        SymbolTable temp = stack.get(stack.size() - 1);
        stack.remove(stack.size() - 1);
        frameName.remove(frameName.size() - 1);
        return temp;
    }

    public boolean hasVariable(String name) {
        for (int i = stack.size() - 1; i >= 0; i--) {
            SymbolTable table = stack.get(i);
            if (table.hasVariable(name)) {
                return true;
            }
        }

        return false;
    }

    public String variableType(String name) {
        for (int i = stack.size() - 1; i >= 0; i--) {
            SymbolTable table = stack.get(i);
            if (table.hasVariable(name)) {
                return table.getVariable(name).varType;
            }
        }

        return null;
    }

    public void addVariable(String name, String type, String flag, int offset) {
        SymbolTable table = stack.get(stack.size() - 1);
        table.addVariable(name, type, flag, offset);
    }
    
    public VariableSymbol getVariable(String name) {
        for (int i = stack.size() - 1; i >= 0; i--) {
            SymbolTable table = stack.get(i);
            if (table.hasVariable(name)) {
                return table.getVariable(name);
            }
        }

        return null;
    }

    public int getVariableOffset(String name) {
        VariableSymbol item = this.getVariable(name);
        return item.offset;
    }
    public String getVariableFlag(String name) {
        for (int i = stack.size() - 1; i >= 0; i--) {
            SymbolTable table = stack.get(i);
            if (table.hasVariable(name)) {
                return table.getVariable(name).flag;
            }
        }

        return "";
    }

    public String getFullVariableName(String name) {
        int i;
        for (i = stack.size() - 1; i >= 0; i--) {
            SymbolTable table = stack.get(i);
            if (table.hasVariable(name)) {
                break;
            }
        }

        StringBuilder temp = new StringBuilder();
        for (int j = 1; j <= i; j++) {
            temp.append(frameName.get(j)).append(".");
        }

        temp.append(name);

        return new String(temp);
    }

    public void addFunction(String name, String retType, ArrayList<String> paramsType) {
        SymbolTable table = stack.get(stack.size() - 1);
        table.addFunction(name, retType, paramsType);
    }

    public FunctionSymbol getFunction(String name) {
        for (int i = stack.size() - 1; i >= 0; i--) {
            SymbolTable table = stack.get(i);
            if (table.hasFunction(name)) {
                return table.getFunction(name);
            }
        }

        return null;
    }

    /*
    public void addArray(String name, ArrayList<Integer> lens, String type) {
        SymbolTable table = stack.get(stack.size() - 1);
        table.addArray(name, lens, type);
    }

    public String getFullArrayName(String name) {
        int i;
        for (i = stack.size() - 1; i >= 0; i--) {
            SymbolTable table = stack.get(i);
            if (table.hasArray(name)) {
                break;
            }
        }

        StringBuilder temp = new StringBuilder();
        for (int j = 1; j <= i; j++) {
            temp.append(frameName.get(j)).append(".");
        }

        temp.append(name);

        return new String(temp);
    }

    public ArraySymbol getArray(String name) {
        for (int i = stack.size() - 1; i >= 0; i--) {
            SymbolTable table = stack.get(i);
            if (table.hasArray(name)) {
                return table.getArray(name);
            }
        }

        return null;
    }
    */
}
