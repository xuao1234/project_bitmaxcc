package bit.minisys.minicc.symbol;

public class VariableSymbol extends Symbol {
    public String varType;
    public String flag;
    public int offset = 0;
}