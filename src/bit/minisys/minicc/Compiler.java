package bit.minisys.minicc;

public class Compiler {
	
	/**
	 * @param args
	 * @throws Exception 
	 */
	public static void main(String[] args) throws Exception {		
		if(args.length != 4  && args.length != 5){
			System.out.println(args.length);
			usage();
			return;
		}
		
		String file = args[2];
		System.out.println(file);
		if(!file.endsWith(".c")){
			System.out.println("Incorrect input file:" + file);
			return;
		}
		
		String out_path = args[3];
		
		MiniCCompiler cc = new MiniCCompiler();
		System.out.println("Start to compile ...");
		cc.run(file, out_path);
		System.out.println("Compiling completed!");
	}
	
	public static void usage(){
		System.out.println("USAGE: compiler -S -o testcase.s testcase.sy [-O2]");
	}
}
