		AREA gdata, DATA, READWRITE

		AREA function, CODE, READONLY
		ENTRY
start
main_
		push {fp, lr}
		mov fp, sp
		sub sp, sp, #4
		sub sp, sp, #4
		sub sp, sp, #4
		mov r6, #0
		str r6, [fp, #-4]
		mov r6, #1
		str r6, [fp, #-8]
		mov r6, #2
		str r6, [fp, #-12]
		mov r6, #3
		push {r6}
		ldr r6, [fp, #-12]
		pop {r7}
		add r6, r6, r7
		push {r6}
		ldr r6, [fp, #-8]
		push {r6}
		ldr r6, [fp, #-4]
		pop {r7}
		add r6, r6, r7
		pop {r7}
		add r6, r6, r7
		str r6, [fp, #-12]
		mov r6, #0
		mov r0, r6
		add sp, sp, #12
		pop {fp, pc}
stop
		B stop

END