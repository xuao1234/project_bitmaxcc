# Project BitMaxCC

## Log Rec

### Ver 1.0

* for 2021.5.26
* 建仓入仓

### Ver 1.1

* for 2021.5.27
* 加入 ./src/test 测试文件


### Ver 2.1

* for 2021.5.28
* 更改词法分析.txt，内容是tokens和name的对应关系

### Ver 2.1.1

* for 2021.5.28
* 添加misc文件夹存储杂项
* 在misc文件夹中添加用户测试脚本，用于进行模块测试
* 在src文件夹中添加test2测试用例

### Ver 2.2

* for 2021.5.29
* misc中更新了测试脚本，具体使用说明见对应文件夹中的md文件
* scanner中添加scanner.cpp、.h和.exe文件，初步完成词法分析
* src文件夹中添加test_scanner、test2_scanner文件夹，作为测试用例对应的单元测试结果


### Ver 3.1

* for 2021.6.3
* 加入Clion项目，构建文法类



### Ver 4.1 
* for 2021 6.10
* 

### Ver 1.0
* for 2021 5.26
* 建库入仓

### Ver 1.1
* for 2021.5.27
* 初步建立词法分析模块，加入Tokenizer.cpp、Tokenizer.h

### Ver 1.2
* for 2021.5.28
* 初步建立语法分析模块，加入AST.cpp和AST.h

### Ver 1.3
* for 2021.5.29
* 初步建立语义分析模块，加入Symbol.cpp、Symbol.h

### ver1.4
* for 2021.5.30
* 初步建立目标代码生成模块，加入Assembly.cpp和Assembly.h

### Ver 2.0
* for 2021 6.3
* 初步完成词法分析模块，向Tokenizer.h加入Tokenizer类
* 添加Utils.cpp, Utils.h文件以实现一些杂项函数功能

### Ver 2.1
* for 2021 6.5
* 在Tokenizer.cpp中加入若干属性和函数
* 在Utils.cpp中加入函数stos，allocator类，枚举类型Token_type

### Ver 3.0 
* for 2021 6.10
* 初步完成语法分析模块，向AST.h加入基类ExprAST

### Ver 3.1
* for 2021 6.12
* 向AST.h加入派生类并完成相关的功能函数

### Ver 4.0
* for 2021 6.16
* 初步完成语义分析模块，向Symbol.h加入SymTable类

### Ver 4.1
* for 2021 6.16
* 向utils.cpp加入isSymbol函数

### Ver 5.0 
* for 2021.6.24
* 建立regcontrol.cpp和asmhelper来辅助生成asm代码

### Ver 5.1
* for 2021.6.29
* 向regcontrol中加入register类

### Ver 5.2
* for 2021.7.1
* 向assembly中加入Getarmcode类

### Ver 6.0
* for 2021.7.5
* 初步完成测试